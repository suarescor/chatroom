﻿using ChatRoom.Common.Helpers;
using ChatRoom.Common.RabbitMqHelper;
using ChatRoom.Services.Dto;
using ChatRoom.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChatRoom.Web.Controllers
{
    public class PostsController : ApiController
    {
        IPostService _postService;
        public PostsController(IPostService postService)
        {
            _postService = postService;
        }


        // GET: api/Posts
        public IEnumerable<PostDtoOut> Get()
        {
            return _postService.GetPosts();
        }

        // GET: api/Posts/5
        public IEnumerable<PostDtoOut> Get(DateTime dateTime)
        {
            return _postService.GetPosts(dateTime);
        }

        // POST: api/Posts
        [HttpPost]
        public PostDtoOut Post([FromBody] PostDtoIn value)
        {
            return _postService.SavePost(value);
        }

        // POST: api/Receive
        [HttpPost]
        [Route("api/Receive")]
        public PostDtoOut Receive([FromBody] PostDtoIn value)
        {
            return _postService.Refresh();
        }

    }
}
