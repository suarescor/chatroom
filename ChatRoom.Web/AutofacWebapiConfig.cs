﻿using Autofac;
using Autofac.Integration.WebApi;
using ChatRoom.Business.BusinessLayer;
using ChatRoom.Business.Interfaces;
using ChatRoom.Repository.DAL;
using ChatRoom.Repository.Interfaces;
using ChatRoom.Repository.Repository;
using ChatRoom.Services.Interfaces;
using ChatRoom.Services.ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;

namespace AutoFacWithWebAPI.App_Start
{
    public class AutofacWebapiConfig
    {

        public static IContainer Container;

        public static void Initialize(HttpConfiguration config)
        {
            Initialize(config, RegisterServices(new ContainerBuilder()));
        }


        public static void Initialize(HttpConfiguration config, IContainer container)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static IContainer RegisterServices(ContainerBuilder builder)
        {
            //Register your Web API controllers.  
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<PostService>()
                   .As<IPostService>()
                   .InstancePerRequest();

            builder.RegisterType<PostRepository>()
                   .As<IPostRepository>()
                   .InstancePerRequest();

            builder.RegisterType<PostBL>()
                   .As<IPostBL>()
                   .InstancePerRequest();

            builder.RegisterType<ChatRoomContext>()
                   .As<ChatRoomContext>()
                   .InstancePerRequest();

            //Set the dependency resolver to be Autofac.  
            Container = builder.Build();

            return Container;
        }

    }
}