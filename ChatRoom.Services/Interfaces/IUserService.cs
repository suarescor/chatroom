﻿using ChatRoom.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.Services.Interfaces
{
    public interface IUserService
    {
        UserDtoOut Login(UserDtoIn user);

    }
}
