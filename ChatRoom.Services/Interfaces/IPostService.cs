﻿using ChatRoom.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.Services.Interfaces
{
    public interface IPostService
    {
        PostDtoOut SavePost(PostDtoIn post);
        IList<PostDtoOut> GetPosts();
        IList<PostDtoOut> GetPosts(DateTime dateTime);

        PostDtoOut Refresh();
    }
}
