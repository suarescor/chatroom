﻿using ChatRoom.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.Services.Dto
{
    [DataContract]
    public class PostDtoOut
    {
        [DataMember(Name = "username")]
        public string Username { get; set; }

        [DataMember(Name = "timestamp")]
        public DateTime Timestamp { get; set; }

        [DataMember(Name = "message")]
        public string Message { get; set; }

        [DataMember(Name = "messageType")]
        public MessageType MessageType { get; set; }
    }
}
