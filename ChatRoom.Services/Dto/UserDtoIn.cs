﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.Services.Dto
{
    public class UserDtoIn
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
