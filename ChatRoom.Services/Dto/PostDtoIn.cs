﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.Services.Dto
{
    [DataContract]
    public class PostDtoIn
    {
        [DataMember(Name ="userId")]
        public int UserId { get; set; }

        [DataMember(Name = "username")]
        public string Username { get; set; }

        [DataMember(Name = "message")]
        public string Message { get; set; }
    }
}
