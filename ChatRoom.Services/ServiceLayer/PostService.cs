﻿using ChatRoom.Business.Interfaces;
using ChatRoom.Common.Helpers;
using ChatRoom.Common.RabbitMqHelper;
using ChatRoom.Domain.Enums;
using ChatRoom.Services.Dto;
using ChatRoom.Services.Interfaces;
using ChatRoom.Services.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.Services.ServiceLayer
{
    public class PostService : IPostService
    {
        IPostBL _bl;
        private const string codeRequest = "/stock=";
        public PostService(IPostBL bl)
        {
            _bl = bl;
        }
        public IList<PostDtoOut> GetPosts()
        {
            return _bl.GetPosts().MapToDtoOut();
        }

        public IList<PostDtoOut> GetPosts(DateTime dateTime)
        {
            return _bl.GetPostsFromDateTime(dateTime).MapToDtoOut();
        }

        public PostDtoOut SavePost(PostDtoIn post)
        {
            MessageType type = MessageType.Post;
            if (string.IsNullOrEmpty(post.Message))
            {
                type = MessageType.Error;
            }
            else if (post.Message.ToLower().StartsWith(codeRequest))
            {
                post.Message = post.Message.Replace(codeRequest, string.Empty);
                type = MessageType.Automated;
            }


            return _bl.SavePost(post.MapToEntity(type)).MapToDtoOut();
        }

        public PostDtoOut Refresh()
        {
            RabbitMqConnection connection = new RabbitMqConnection();
            var result = connection.Get();
            if (string.IsNullOrEmpty(result)) return null;
            var post = new MessageConverter().ConvertToPost(result);
            return post.MapToDtoOut();
        }
    }
}
