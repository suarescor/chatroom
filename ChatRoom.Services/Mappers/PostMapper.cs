﻿using ChatRoom.Domain.Entities;
using ChatRoom.Domain.Enums;
using ChatRoom.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.Services.Mappers
{
    public static class PostMapper
    {
        public static PostDtoOut MapToDtoOut(this Post post)
        {
            return new PostDtoOut
            {
                Message = post.Message,
                MessageType = post.MessageType,
                Username = post.User.Username,
                Timestamp = post.Timestamp
            };
        }

        public static IList<PostDtoOut> MapToDtoOut(this IList<Post> posts)
        {
            var list = new List<PostDtoOut>();
            foreach (var post in posts)
            {
                list.Add(post.MapToDtoOut());
            }
            return list;
        }

        public static Post MapToEntity(this PostDtoIn post, MessageType messageType)
        {
            return new Post
            {
                Message = post.Message,
                Timestamp = DateTime.Now,
                User = new User { Username = post.Username, Id = post.UserId },
                Username = post.Username,
                MessageType = messageType
            };
        }
    }
}
