﻿using ChatRoom.Domain.Entities;
using ChatRoom.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.MessageBot.Implementations
{
    public class StooqBot : ChatRoom.Domain.Interfaces.IMessenger
    {
        private readonly string Url = "https://stooq.com/q/l/?s={0}&f=sd2t2ohlcv&h&e=csv";
        private static string NotFound = "N/D";
        public Post PostMessage(Post post)
        {
            try
            {
                var response = new Post();
                response.Message = DownloadString(string.Format(Url, post.Message));
                response.HttpCode = HttpStatusCode.OK;
                response.Timestamp = DateTime.Now;
                response.User = new User{Id=0, Username="ChatRoom bot"};
                response.MessageType = Domain.Enums.MessageType.Automated;
                return response;
            }
            catch (WebException ex)
            {
                return new Post
                {
                    Message = ex.Message,
                    HttpCode = HttpStatusCode.InternalServerError,
                    Timestamp = DateTime.Now,
                    MessageType = Domain.Enums.MessageType.Error
                };
            }
        }

        private static string DownloadString(string address)
        {
            WebClient client = new WebClient();
            string reply = client.DownloadString(address);
            return GetCsvInformation(reply);
        }

        private static string GetCsvInformation(string csv)
        {
            string[] lines = csv.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            string[] columns = lines[1].Split(',');
            if(columns[6] == NotFound)
                return string.Format("{0} didn't produce any results", columns[0]);
            return string.Format("{0} quote is ${1} per share.", columns[0], columns[6]);
        }

    }
}
