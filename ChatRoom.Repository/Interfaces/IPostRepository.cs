﻿using ChatRoom.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.Repository.Interfaces
{
    public interface IPostRepository
    {
        IList<Post> GetPosts();
        IList<Post> GetPostsAfterDate(DateTime dateTime);
        Post SavePost(Post post);
    }
}
