﻿using ChatRoom.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.Repository.Interfaces
{
    public interface IUserRepository
    {
        User GetUserByUsername(string username);
        User SaveUser(User user);
    }
}
