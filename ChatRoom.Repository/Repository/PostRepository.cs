﻿using ChatRoom.Domain.Entities;
using ChatRoom.Repository.DAL;
using ChatRoom.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.Repository.Repository
{
    public class PostRepository : IPostRepository
    {
        private ChatRoomContext _context;

        public PostRepository(ChatRoomContext context)
        {
            _context = context;
        }
        public IList<Post> GetPosts()
        {
            return _context.Posts.Include(x => x.User).OrderByDescending(x => x.Timestamp).Take(50).ToList();
        }

        public IList<Post> GetPostsAfterDate(DateTime dateTime)
        {
            return _context.Posts.OrderByDescending(x => x.Timestamp).Where(x => x.Timestamp >= dateTime).Take(50).ToList();
        }

        public Post SavePost(Post post)
        {
            _context.Posts.Add(post);
            _context.SaveChanges();
            return post;
        }
    }
}
