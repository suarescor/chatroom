﻿using ChatRoom.Domain.Entities;
using ChatRoom.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.Repository.DAL
{
    public class ChatRoomContext : DbContext
    {

        public ChatRoomContext() : base("ChatRoomDbContext")
        {
        }

        public DbSet<Post> Posts { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Post>()
                .HasRequired(s => s.User).WithMany().HasForeignKey(x=>x.UserId);
        }
    }
}
