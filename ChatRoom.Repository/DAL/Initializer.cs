﻿using ChatRoom.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.Repository.DAL
{
    public class Initializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<ChatRoomContext>
    {
        protected override void Seed(ChatRoomContext context)
        {
            var users = new List<User>
            {
            new User{Username="Carson",Password="Alexander"},
            new User{Username="Meredith",Password="Alonso"},
            new User{Username="Arturo",Password="Anand"},
            new User{Username="Gytis",Password="Barzdukas"},
            new User{Username="Yan",Password="Li"},
            new User{Username="Peggy",Password="Justice"},
            new User{Username="Laura",Password="Norman"},
            new User{Username="Nino",Password="Olivetto"}
            };

            users.ForEach(s => context.Users.Add(s));
            context.SaveChanges();
            var posts = new List<Post>
            {
            new Post{UserId=1,Message="Chemistry",Timestamp=DateTime.Now},
            new Post{UserId=2,Message="Microeconomics",Timestamp=DateTime.Now.AddHours(-1)},
            new Post{UserId=3,Message="Macroeconomics",Timestamp=DateTime.Now.AddHours(-2)},
            new Post{UserId=4,Message="Calculus",Timestamp=DateTime.Now.AddHours(-4)},
            new Post{UserId=5,Message="Trigonometry",Timestamp=DateTime.Now.AddHours(-8)},
            new Post{UserId=4,Message="Composition",Timestamp=DateTime.Now.AddHours(-10)},
            new Post{UserId=4,Message="Literature",Timestamp=DateTime.Now.AddHours(-12)}
            };
            posts.ForEach(s => context.Posts.Add(s));
            context.SaveChanges();

        }
    }
}
