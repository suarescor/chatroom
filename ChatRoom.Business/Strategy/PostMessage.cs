﻿using ChatRoom.Domain.Entities;
using ChatRoom.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.Business.Strategy
{
    public class PostMessage
    {
        readonly IMessenger _messenger;
        public PostMessage(IMessenger messenger)
        {
            _messenger = messenger;
        }

        public Post Send(Post post)
        {
           return _messenger.PostMessage(post);
        }
    }
}
