﻿using ChatRoom.Common.Helpers;
using ChatRoom.Common.RabbitMqHelper;
using ChatRoom.Domain.Entities;
using ChatRoom.Domain.Interfaces;
using ChatRoom.Repository.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.Business.Strategy.Implementation
{
    public class Persist : IMessenger
    {
        readonly IPostRepository _postRepository;

        public Persist(IPostRepository postRepository)
        {
            _postRepository = postRepository;
        }

        public Post PostMessage(Post post)
        {
            RabbitMqConnection connection = new RabbitMqConnection();
            connection.Send(new MessageConverter().ConvertToMessage(post));
            return _postRepository.SavePost(post);
        }
    }
}
