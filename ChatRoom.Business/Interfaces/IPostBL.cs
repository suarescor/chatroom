﻿using ChatRoom.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.Business.Interfaces
{
    public interface IPostBL
    {
        IList<Post> GetPosts();
        IList<Post> GetPostsFromDateTime(DateTime datetime);
        Post SavePost(Post post);
    }
}
