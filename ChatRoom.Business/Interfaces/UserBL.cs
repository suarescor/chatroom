﻿using ChatRoom.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.Business.Interfaces
{
    public interface UserBL
    {
        User LoginUser(User user);
        User SaveUser(User user);
    }
}
