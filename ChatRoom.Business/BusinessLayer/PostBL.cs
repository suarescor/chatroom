﻿using ChatRoom.Business.Interfaces;
using ChatRoom.Business.Strategy;
using ChatRoom.Business.Strategy.Implementation;
using ChatRoom.Common.Helpers;
using ChatRoom.Domain.Entities;
using ChatRoom.Domain.Enums;
using ChatRoom.MessageBot.Implementations;
using ChatRoom.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.Business.BusinessLayer
{
    public class PostBL : IPostBL
    {
        IPostRepository _postRepository;
        public PostBL(IPostRepository postRepository)
        {
            _postRepository = postRepository;
        }

        public IList<Post> GetPosts()
        {
            return _postRepository.GetPosts();
        }

        public IList<Post> GetPostsFromDateTime(DateTime datetime)
        {
            return _postRepository.GetPostsAfterDate(datetime);
        }

        public Post SavePost(Post post)
        {
            post.Message = StringHelper.Sanitize(post.Message);
            if (string.IsNullOrEmpty(post.Message))
                post.MessageType = MessageType.Error;

            switch (post.MessageType)
            {
                case MessageType.Post:
                    var messenger = new PostMessage(new Persist(_postRepository));
                    return messenger.Send(post);
                case MessageType.Automated:
                    var messengerBot = new PostMessage(new StooqBot());
                    return messengerBot.Send(post);
                case MessageType.Error:
                    return new Post()
                    {
                        HttpCode = System.Net.HttpStatusCode.BadRequest,
                        Message = "The code you entered is not valid. Please check your input and try again."
                    };
            }
            return null;
        }
    }
}
