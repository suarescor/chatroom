﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.Domain.Enums
{
    public enum MessageType
    {
        Post = 0,
        Automated = 1,
        Error = 2
    }
}
