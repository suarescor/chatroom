﻿using ChatRoom.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.Domain.Interfaces
{
    public interface IMessenger
    {
        Post PostMessage(Post post);
    }
}
