﻿using ChatRoom.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.Domain.Entities
{
    public class Post
    {
        public int Id { get; set; }

        //[ForeignKey("User")]
        public int UserId { get; set; }
        public User User { get; set; }

        public DateTime Timestamp { get; set; }
        public string Message { get; set; }

        [NotMapped]
        public MessageType MessageType;

        [NotMapped]
        public string Username { get; set; }

        [NotMapped]
        public HttpStatusCode HttpCode { get; set; }

    }
}
