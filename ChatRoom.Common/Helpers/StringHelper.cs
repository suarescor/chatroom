﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.Common.Helpers
{
    public static class StringHelper
    {
        public static string Sanitize(string dirtyString)
        {
            HashSet<char> removeChars = new HashSet<char>("&^$#@!()+-,:;<>’\'-_*");
            StringBuilder result = new StringBuilder(dirtyString.Length);
            foreach (char c in dirtyString)
                if (!removeChars.Contains(c))
                    result.Append(c);
            return result.ToString();
        }
    }
}
