﻿using ChatRoom.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoom.Common.Helpers
{
    public class MessageConverter
    {
        public string ConvertToMessage(Post post)
        {
            return $"{post.UserId.ToString()}|{post.User.Username}|{post.Message}|{post.Timestamp.ToString()}";
        }

        public Post ConvertToPost(string message)
        {
            var postInfo = message.Split('|');
            return new Post
            {
                UserId = int.Parse(postInfo[0]),
                User = new User
                {
                    Id = int.Parse(postInfo[0]),
                    Username = postInfo[1]
                },
                Message = postInfo[2],
                Timestamp = DateTime.Parse(postInfo[3])
            };
        }
    }
}
