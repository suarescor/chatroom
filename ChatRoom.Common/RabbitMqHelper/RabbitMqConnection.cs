﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;

namespace ChatRoom.Common.RabbitMqHelper
{

    public class RabbitMqConnection
    {
        static readonly ConnectionFactory connFactory = new ConnectionFactory();
        private static string url = ConfigurationManager.AppSettings["CLOUDAMQP_URL"];
        static readonly string QueueName = "queue2";

        public void Send(string message)
        {
            connFactory.Uri = new Uri(url.Replace("amqp://", "amqps://"));

            // create a connection and open a channel, dispose them when done
            using (var conn = connFactory.CreateConnection())
            using (var channel = conn.CreateModel())
            {
                // The message we want to put on the queue
                //var message = DateTime.Now.ToString("F");
                // the data put on the queue must be a byte array
                var data = Encoding.UTF8.GetBytes(message);
                // ensure that the queue exists before we publish to it
                var queueName = QueueName;
                bool durable = true;
                bool exclusive = false;
                bool autoDelete = false;
                channel.QueueDeclare(queueName, durable, exclusive, autoDelete, null);
                // publish to the "default exchange", with the queue name as the routing key
                var exchangeName = "";
                var routingKey = QueueName;
                channel.BasicPublish(exchangeName, routingKey, null, data);
            }
        }

        public string Get()
        {
            connFactory.Uri = new Uri(url.Replace("amqp://", "amqps://"));

            using (var conn = connFactory.CreateConnection())
            using (var channel = conn.CreateModel())
            {
                // ensure that the queue exists before we access it
                channel.QueueDeclare(QueueName, true, false, false, null);
                var queueName = QueueName;
                // do a simple poll of the queue
                var data = channel.BasicGet(queueName, false);
                // the message is null if the queue was empty
                if (data == null) return null;
                // convert the message back from byte[] to a string
                var message = Encoding.UTF8.GetString(data.Body);
                // ack the message, ie. confirm that we have processed it
                // otherwise it will be requeued a bit later
                channel.BasicAck(data.DeliveryTag, false);
                return message;
            }
        }
    }
}